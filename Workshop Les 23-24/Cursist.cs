﻿using System.Windows.Media;

namespace Workshop_Les_23_24
{
    internal class Cursist : Persoon
    {
        //Attributen
        private Guid _cursistennummer;
        //Constructor
        Cursist(string eenVoornaam, string eenAchternaam, ImageSource eenAvatar, Guid eenGuid) : base(eenVoornaam, eenAchternaam, eenAvatar)
        {
            Cursistennummer = eenGuid;
        }
        Cursist() { }
        //Properties
        public Guid Cursistennummer { get { return _cursistennummer; } set { _cursistennummer = value; } }
        //Methodes
        public override string Details()
        {
            return base.Details() + " " + Cursistennummer;
        }
    }
}
