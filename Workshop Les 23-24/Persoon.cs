﻿using System.Windows.Media;

namespace Workshop_Les_23_24
{
    internal class Persoon
    {
        //Attributen
        private string _voornaam;
        private string _achternaam;
        private ImageSource _imageSource;
        //Constructor
        public Persoon() { }
        public Persoon(string eenVoornaam, string eenAchternaam, ImageSource eenAvatar)
        {
            Voornaam = eenVoornaam;
            Achternaam = eenAchternaam;
            Avatar = eenAvatar;

        }
        public Persoon(string eenVoornaam, string eenAchternaam)
        {
            Voornaam = eenVoornaam;
            Achternaam = eenAchternaam;
        }
        //Properties
        public string Voornaam { get { return _voornaam; } set { _voornaam = value; } }
        public string Achternaam { get { return _achternaam; } set { _achternaam = value; } }
        public ImageSource Avatar { get { return _imageSource; } set { _imageSource = value; } }
        //Methode
        public virtual string Details()
        {
            return Avatar + " " + Voornaam + " " + Achternaam;
        }
    }
}
