﻿using System.Windows;

namespace Workshop_Les_23_24
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void BtnOpslagen_Click(object sender, RoutedEventArgs e)
        {
            List<Persoon> listPersonen = new List<Persoon>();
            if (!txtAchternaam.Text.Any(char.IsDigit) && !txtVoornaam.Text.Any(char.IsDigit) && !String.IsNullOrWhiteSpace(txtAchternaam.Text) && !String.IsNullOrWhiteSpace(txtVoornaam.Text))
            {

                MessageBox.Show("Welkom " + txtVoornaam.Text + " " + txtAchternaam.Text + " !", ".Net traject", MessageBoxButton.OK, MessageBoxImage.Information);
                Persoon nieuwPersoon = new Persoon(txtVoornaam.Text, txtAchternaam.Text);
                listPersonen.Add(nieuwPersoon);
                txtAchternaam.Clear();
                txtVoornaam.Clear();
                foreach (Persoon persoon in listPersonen)
                {
                    cmbCursistent.Items.Add(persoon.Details());
                }
            }
            else
            {
                MessageBox.Show("Vul alsjeblieft een geldig naam in en laat geen veld leeg. (bv: Illya Verheyden)", "Foutmelding", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }
    }
}